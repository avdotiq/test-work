var count = 0;

function vs() {
    var contener = document.getElementById("open");
    var imgBckg = document.getElementById("img-change");
    imgBckg.classList.remove("img-down");
    if (count >= 1) {
        count--;
        contener.classList.remove("hidden-open");
        imgBckg.classList.remove("img-up");
        imgBckg.classList.add("img-down");
    } else if (count < 1) {
        count++;
        contener.classList.add("hidden-open");
        imgBckg.classList.add("img-up");
    }
}

var tog = false;

document.addEventListener( "DOMContentLoaded", function() {
	var elementsScene = document.getElementsByClassName("accord-header-panel");
	for (i=0; i < elementsScene.length; i++) { 
      elementsScene[i].addEventListener("click", toogler);
	}
})

function toogler() {
    el = event.currentTarget;
    var newclass = el.lastElementChild;
    if (tog === true) {
        tog = false;
        newclass.classList.remove("accord-open");
    } else if (tog === false) {
        tog = true;
        newclass.classList.add("accord-open");
    }
}